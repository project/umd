function ump_distribute_commission() { 
    Swal.fire({
        title: Drupal.t('Are you sure?'),
        text: Drupal.t("You want to Distribute Commission."),
        type: Drupal.t('warning'),
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonClass: 'let-m-2 let-btn-sm',
        cancelButtonColor: '#d33',
        cancelButtonClass: 'let-m-2 let-btn-sm',
        confirmButtonText: Drupal.t('Distribute Commission')
    }).then((result) => { 
        if (result.value) {   
            jQuery.ajax({        
                url: "distribute_commission_function",        
                beforeSend:function(){
                    jQuery('#run_distribute_loader').css('display','block');
                },
                success: function (data) {
                    var obj = jQuery.parseJSON(data);    
                    jQuery('#run_distribute_loader').css('display','none'); 
                    if(obj.status==true){
                        Swal.fire(
                            Drupal.t('Success'),
                            obj.message,                             
                             'success'
                            ).then((result) => {
                                location.reload();
                            });                   
                        }               
                }
            });
        }else{
            Swal.fire(
                Drupal.t('Cancle'),
                Drupal.t('Commission distribution canceled.'),
                 'error'
            );
        }
    }); 
}
function ump_run_payout() {
    Swal.fire({
        title: Drupal.t('Are you sure?'),
        text: Drupal.t("You want to run Payout."),
        type: Drupal.t('warning'),
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonClass: 'let-m-2 let-btn-sm',
        cancelButtonColor: '#d33',
        cancelButtonClass: 'let-m-2 let-btn-sm',
        confirmButtonText: Drupal.t('Run Payout')
    }).then((result) => { 
        if (result.value) {      
                jQuery.ajax({          
                    url: "run_payout_function",
                    beforeSend:function()        {
                    jQuery('#run_payout_loader').css('display','block');
                },
                success: function (data) {           
                    jQuery('#run_payout_loader').css('display','none');                    
                    var obj = jQuery.parseJSON(data);         
                    if(obj.status==true ){
                        Swal.fire(
                            Drupal.t('Success'),
                            obj.message,                             
                             'success'
                            ).then((result) => {
                                location.reload();
                            }); 
                        }else{
                            Swal.fire(
                                Drupal.t('Success'),
                                Drupal.t('record is empty'),                             
                                'success'
                                ).then((result) => {
                                    location.reload();
                                });                            
                        }
                        
                    }
                });
        }
        else{
            Swal.fire(
                Drupal.t('Cancle'),
                Drupal.t('Run Payout Canceled.'),
                 'error'
            );
        }
    }); 
}
jQuery('#ump_account_details_form').submit(function (e) {
    e.preventDefault();
    var accdata = $(this).serialize();     
    jQuery.ajax({
        type: 'POST',
        context: this,
        url: 'add_bank_detail',
        beforeSend:function() {
            jQuery('#bank_loader').css('display','block');
            },
            data: accdata,        
            success: function (data) {                
            jQuery('#bank_loader').css('display','none');
            var obj = jQuery.parseJSON(data);
            if(obj.status==true)
            { Swal.fire(
                Drupal.t('Success'),
                obj.message,                             
                 'success'
                ).then((result) => {
                    location.reload();
                });               
            }else{
                $.each(obj.error, function (key, value) {
                    $('#' + key).html(value);
                });               
            }        
               
        }
    });
}); 

function ump_withdrwal_request(){     
    var amount = jQuery('#withdrawal_amount').val();
    Swal.fire({
        title: Drupal.t('Are you sure?'),
        text: Drupal.t("You want to Withdrawal Amount."),
        type: Drupal.t('warning'),
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonClass: 'let-m-2 let-btn-sm',
        cancelButtonColor: '#d33',
        cancelButtonClass: 'let-m-2 let-btn-sm',
        confirmButtonText: Drupal.t('Yes, Withdrawal it!')
    }).then((result) => { 
        if (result.value) {      
            jQuery.ajax({
                type: 'POST',
                context: this,
                url: 'ump_withdrawal_amount_by_user',
                data:{'amount':amount},
                beforeSend:function() {
                    jQuery('#user_request_amount_loader').css('display','block');
                    },
                    success: function (data) {             
                        jQuery('#user_request_amount_loader').css('display','none');
                    var obj = jQuery.parseJSON(data);
                    if (obj.status == true) {
                        Swal.fire(
                            Drupal.t('Success'),
                            obj.message,                             
                            'success'
                            ).then((result) => {
                                location.reload();
                            });                 
                    }else{
                        Swal.fire(
                            Drupal.t('Please Check It !'),
                            obj.message,                             
                            'error'
                            ).then((result) => {
                                location.reload();
                            });
                    }                  
                }
            });
        }else{
            Swal.fire(
                Drupal.t('Cancle'),
                Drupal.t('Withdrawal Amount Canceled.'),
                'error'
            );
        }
        
    });
}

jQuery('#umw_payment_form').submit(function (e) {
    e.preventDefault();
    var form = $(this);
    var postdata = form.serialize(); 
    Swal.fire({
        title: Drupal.t('Are you sure?'),
        text: Drupal.t("You want to Transfer Amount."),
        type: Drupal.t('warning'),
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonClass: 'let-m-2 let-btn-sm',
        cancelButtonColor: '#d33',
        cancelButtonClass: 'let-m-2 let-btn-sm',
        confirmButtonText: Drupal.t('Yes, Transfer it!')
    }).then((result) => { 
        if (result.value) {      
                jQuery.ajax({   
                    type:'POST'             ,
                    url: 'transfer_user_amount',
                    data: postdata,      
                    beforeSend:function() {
                        jQuery('#admin_transfer_amount_loader').css('display','block');
                    },
                    success: function (data) {            
                        jQuery('#admin_transfer_amount_loader').css('display','none');
                        var obj = jQuery.parseJSON(data);             
                        if(obj.status==true)
                        {
                            Swal.fire(
                                Drupal.t('Success'),
                                obj.message,                             
                                'success'
                                ).then((result) => {
                                    location.reload();
                                }); 
                        
                        }else{
                            Swal.fire(
                                Drupal.t('Please Check It !'),
                                obj.error,                             
                                'error'
                                ).then((result) => {
                                    location.reload();
                                });                            
                        }
                         
                    }
                });
        }else{
            Swal.fire(
                Drupal.t('Cancle'),
                Drupal.t('Transfer Amount Canceled.'),
                'error'
            );
        }
    });
});
 
 
 
     
 