// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

// Pie Chart Example
// (function ($,Drupal,drupalSettings) {
//     Drupal.behaviors.test = {
//       attach: function(context,settings) {
var payout=drupalSettings.unilevelmlm.payout;
var ctx = document.getElementById("myPieChart");
var myPieChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ["Join Commission", "Referral Commission", "Level Commission", "Ragular Bonus"],
        datasets: [{
            data: [payout.join_commission, payout.ref_commission, payout.level_commission, payout.ragular_bonus],
            backgroundColor: ['#4e73df', '#1cc88a', '#36b9cc', '#f75262', '#ffc107'],
            hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf', '#dd2f40 ', '#f6c23e'],
            hoverBorderColor: "rgba(234, 236, 244, 1)",  

        }],
    },
    options: {
        maintainAspectRatio: true,
        tooltips: {
            backgroundColor: "rgb(255,255,255)",
            bodyFontColor: "#858796",
            borderColor: '#dddfeb',
            borderWidth: 1,
            xPadding: 10,
            yPadding: 10,
            displayColors: false,
            caretPadding: 10,
        },
        legend: {
            display: false
        },
        cutoutPercentage: 80,
    },
});
// }
// }
// })(jQuery,Drupal,drupalSettings);

