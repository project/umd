<?php
namespace Drupal\unilevelmlm\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\unilevelmlm\UmpClass\Ump ; 
use Drupal\user\Entity\User; 
use Drupal\Core\Ajax\ReplaceCommand; 
use Drupal\Core\Ajax\InsertCommand;
 

class EpinGenerateForm extends ConfigFormBase {

 
	public function getFormId()
	{
		return 'unilevelmlm_epingenerate_admin_config_form';
	}

	protected function getEditableConfigNames(){	 
	 
		return [
			 'unilevelmlm.epingenerate'
		];

	}

	public function buildForm(array $form , FormStateInterface $form_state){	 
		$config = $this->config('unilevelmlm.epingenerate');		
 
		$form['epingenerate'] = [
            '#type' => 'details',
            '#title' =>t('ePins Generate'),
            '#open' => FALSE, 
          ];     
           

		$form['epingenerate']['ump_epin_name']=[
			'#type'=>'textfield',
			'#title'=>t('ePin Name'),
			'#required' => TRUE,
			'#description'=>t('Type your ePin Name'),
			'#default_value'=>$config->get('ump_epin_name'),
			'#placeholder'=>t('Enter ePin Name'),
			'#attributes'	=> array('class' => array('let-form-control w-25')),

		];
	 
		
		$form['epingenerate']['ump_no_of_ePins']=[
			'#type'=>'number',
			'#title'=>t('No of ePins'),
			'#required' => TRUE,
			'#description'=>t('Enter No of ePins'),
			'#default_value'=>$config->get('ump_no_of_ePins'),
			'#placeholder'=>t('No of ePins'),
			'#attributes'	=> array('class' => array('let-form-control w-25')),			 
		];
		 

		$form['epingenerate']['ump_ePin_price']=[
			'#type'=>'number',
			'#title'=>t('ePin Price'),
			'#required' => TRUE,
			'#description'=>t('Enter ePin Price'),
			'#default_value'=>$config->get('ump_ePin_price'),
			'#placeholder'=>t('Enter ePin Price'),
			'#attributes'	=> array('class' => array('let-form-control w-25')),
		];
		 
		
		 

		$form['epingenerate']['submit'] = [
            '#type' => 'submit',
            '#value' => t('Generate epin'),  

        ];

        $form['epingenerate']['submit']['#attributes']['class'][]='button--primary';

        return $form;
	}
	
	 
  

	public function submitForm(array &$form, FormStateInterface $form_state){
		$connection = \Drupal::service('database');
		$ump_ePin_length = \Drupal::config('unilevelmlm.general')->get('ump_epin_length');  
  		$ump_epin_type = 'paid';
  		$ump_ePin_name=$form_state->getValue('ump_epin_name');         
        $ump_epin_price = $form_state->getValue('ump_ePin_price');
        $ump_no_of_epin=$form_state->getValue('ump_no_of_ePins');        
       
        $ump_epin_slug = str_replace(' ', '_', strtolower(preg_replace('/\d+/u', '', $ump_ePin_name)));
        $current_time = \Drupal::time()->getCurrentTime(); 
		$date_output = date('Y-m-d h:i:s', $current_time);
		$messenger = \Drupal::messenger();
		if($ump_no_of_epin>1000)
		{
			return $messenger->addMessage('You can create 1000 ePins on single time', $messenger::TYPE_WARNING);
		}
		 
		if(!empty($ump_ePin_length))
		{
			$master_id = $connection->query("INSERT INTO {ump_epin_master} (epin_name,epin_slug, type,epin_price,status,create_date) VALUES ('$ump_ePin_name','$ump_epin_slug', '$ump_epin_type','$ump_epin_price','0','$date_output')", [], ['return' => Database::RETURN_INSERT_ID]);   
			

	        if($master_id>0)
	        {
	            for($i = 1; $i <= $ump_no_of_epin; $i++) {
	                $epin_no = ump_epin_genarate($ump_ePin_length);		 
	                do {  	            
	            $query = $connection->select('ump_epins', 'ue')
						  ->condition('ue.epin_no', $epin_no, '=')
						  ->fields('ue', ['id'])->range(0,1);
						  $result = $query->execute();
						  $check=0;		   
						  foreach ($result as $record) {
						  	$check++;	 
							}
	                    $flag = 1;
	                    if ($check == 1) {
	                        $epin_no = ump_epin_genarate($ump_ePin_length);
	                        $flag = 0;
	                    }
	                } while ($flag == 0);               

		  			$connection->insert('ump_epins')
		  			->fields([
		  					    'master_id' => $master_id,
							    'epin_no'=> $epin_no,
							    'price' => $ump_epin_price,
							    'type'=> $ump_epin_type,						     		       
							    'date_generated' => $date_output,				   
							  ])
		  			->execute();  		 
	            	 
	            }
				return $this->messenger()->addStatus($this->t('ePins has been Created.'));
	        }        
	 	}		 
	}
}
?>