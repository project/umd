<?php
namespace Drupal\unilevelmlm\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Url;
use Drupal\Core\Routing;
use Drupal\user\RoleInterface;
use Drupal\user\RoleStorageInterface;
use Drupal\user\Entity\User;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InsertCommand;
use Drupal\Core\Ajax\CssCommand;
 

class UmpJoinForm extends ConfigFormBase {

    public function getFormId()
    {
        return 'unilevelmlm_ump_join_config_form';
    }

    protected function getEditableConfigNames()
    {
          
        return [
            'unilevelmlm.ump_join'
        ];
    }
        

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config('unilevelmlm.ump_join'); $site_url=$GLOBALS['base_url'];       
        $form['ump_join'] = array(
            '#type' => 'details',
            '#title' => t('Join Us'),
            '#open' => TRUE, 
          );        
 
        $form['ump_join']['ump_sponsor'] = [
            '#type' => 'textfield',
            '#title' => t('Sponsor'),
            '#required' => TRUE,
            '#default_value' => $config->get('ump_sponsor'),
            '#placeholder'=>t('Fill Sponsor'),
            '#attributes'	=> array('class' => array('form-control w-25')),
            '#ajax'=>[
                'callback'=>[$this, 'check_sponsor_valid'],                                
                'disable-refocus' => TRUE,
                'event' => 'blur',             
                'progress' => [
                    'type' => 'throbber',
                    'message' => $this->t('Verifying Sponsor'),
                  ],
                ],
                '#suffix'=>'<span id="edit-ump-sponsor-result"></span>'
            
        ];
        
        $form['ump_join']['ump_ePin'] = [
            '#type' => 'textfield',
            '#title' => t('ePin'),             
            '#default_value' => $config->get('ump_ePin'),
            '#placeholder'=>t('Fill ePins'),
            '#attributes'	=> array('class' => array('form-control w-25')),
            '#ajax'=>[
                'callback'=>[$this, 'check_ePin_valid'],                                
                'disable-refocus' => TRUE,
                'event' => 'blur',             
                'progress' => [
                    'type' => 'throbber',
                    'message' => $this->t('Verifying ePin'),
                  ],
                ],
                '#suffix'=>'<span id="edit-ump-ePin-result"></span><br>'           
        ];

        $form['ump_join']['actions']['#type'] = 'actions';
        $form['ump_join']['submit'] = [
            '#type' => 'submit',
            '#value' => t('Join User'),
        ];

        $form['ump_join']['submit']['#attributes']['style'][]='background:#6262ce; color:white; height:35px; border:#6262ce; margin:auto';

        $form['ump_join']['login_link'] = [
            '#title' => $this->t('Login'),
            '#type' => 'link',
            '#url' =>$site_url.'urer/login',
          ];
        return $form;
    } 

    function check_sponsor_valid(array &$form, FormStateInterface $form_state){
        $ajax_response = new AjaxResponse();
        $ump_sponsor=$form_state->getValue('ump_sponsor');
        $sponsor_valid=ump_sponsor_exist_function($ump_sponsor);         
        if($sponsor_valid==0)
        {            
            $value='';
            $text=t('Sponsor is not Availble');      
            $css = ['color' => 'red'];
        }else{
            // $form['ump_join']['ump_user_name']['#value']=$user_name;            
            $text=t('Sponsor is Availble');      
            $value=$user_name;
            $css = ['color' => 'green'];
        }  
        $ajax_response->addCommand(new HtmlCommand('#edit-ump-sponsor-result', $text));
        $ajax_response->addCommand(new CssCommand('#edit-ump-sponsor-result', $css));        
        // $ajax_response->addCommand(new HtmlCommand('#edit-ump-user-name', $value));
        return $ajax_response;        
    }

    function check_ePin_valid(array &$form, FormStateInterface $form_state){
        $ajax_response = new AjaxResponse();
        $ump_ePin=$form_state->getValue('ump_ePin');
        $valid_epin=ump_check_epins($ump_ePin);        
        if($valid_epin['status']==false || $valid_epin['status']==0)
        {
            $text=$valid_epin['message'];
            $css = ['color' => 'red'];
        }else{            
            $text=$valid_epin['message'];
            $css = ['color' => 'green'];
        }         
        $ajax_response->addCommand(new HtmlCommand('#edit-ump-ePin-result', $text));
        $ajax_response->addCommand(new CssCommand('#edit-ump-ePin-result', $css));        
        // $ajax_response->addCommand(new HtmlCommand('#edit-ump-user-name', $value));
        return $ajax_response;        
    }  
    

    public function submitForm(array &$form, FormStateInterface $form_state)
    {  
         
        $sponsor_name=$form_state->getValue('ump_sponsor');
        $ePin=$form_state->getValue('ump_ePin');
        $messenger = \Drupal::messenger();
        $connection=\Drupal::service('database');
        $ump_key='user_join_ump';
        $title=t('Join Ump');
        $msg=t('You are join Successfully Ump');
        if(empty(\Drupal::currentUser()->id()))
        {  
            return $messenger->addMessage('You have to Login First', $messenger::TYPE_WARNING);
        }
         
        $user=check_ump_user(\Drupal::currentUser()->id());
        if(!empty($user))
        {
            return $messenger->addMessage('You are already Ump user', $messenger::TYPE_WARNING);
        }        

      
        $sponsor=ump_sponsor_exist_function($sponsor_name);        
        if($sponsor==0)
        {
            return $messenger->addMessage('Sponsor is not Availble !', $messenger::TYPE_WARNING);             
        }
        $valid_epin=ump_check_epins($ePin);
        
        if($valid_epin['status']==false || $valid_epin['status']==0)
        {
            return $messenger->addMessage($valid_epin['message'], $messenger::TYPE_WARNING);             
        }
        $config_refferal = \Drupal::config('unilevelmlm.general')->get('ump_referrals');
        $sponsor_id = $connection->query("SELECT uid FROM {users_field_data} WHERE name=:sponsor_name",['sponsor_name'=>$sponsor_name])->fetchField();
        $referrals = $connection->query("SELECT COUNT(*) FROM {ump_user} WHERE sponsor_key=:sponsor_id",['sponsor_id'=>$sponsor_id])->fetchField();
       
        if (!empty($sponsor_id)) {
            $mlm_key = ump_generateKey();
                do {
                    $count_key = $connection->query("SELECT COUNT(*) FROM {ump_user} 
                                                    WHERE mlm_key = '".$mlm_key."'")->fetchField();
                    $flag = 1;
                    if ($count_key == 1) {
                        $mlm_key = ump_generateKey();
                        $flag = 0;
                    }
                } while ($flag == 0);
                if ($config_refferal == 0) {
                    $parent_key = $sponsor_id;
                } else if ($config_refferal > $referrals) {
                    $parent_key = $sponsor_id;
                } else {
                    $parent_key =  ump_get_parent_key($sponsor_id);
                }
                $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
                $user=User::load(\Drupal::currentUser()->id());                        
                $user->set("langcode", $language);                     
                $user->activate();        
                $user->addRole('ump_user');       
                $user->save();
                $user_id =\Drupal::currentUser()->id();
                // $user_id=$user->id();                
                $mlm_key = ump_generateKey();         
                
                $current_date= date('Y-m-d h:i:s', \Drupal::time()->getCurrentTime());                       
                $n_row = $connection->query("SELECT n_row FROM {ump_user} WHERE user_id='".$parent_key."'")->fetchField();
                $n_row = $n_row + 1;             
                $connection->insert('ump_user')->fields([                 
                'user_id' => $user_id,
                'mlm_key'=> $mlm_key,		 				     
                'parent_key'=> $parent_key,	
                'sponsor_key' =>$sponsor_id,
                'n_row'=>$n_row,			         
                'payment_status' => "2",
                'creation_date'=>$current_date,
                'payment_date'=>$current_date
                ])->execute();
                
                
                ump_insert_hirerchyrecord($user_id, $n_row); 
                
                    if (!empty($ePin)) {                         
                        $ePin_a = $connection->query("SELECT COUNT(*) FROM {ump_epins} WHERE epin_no='".$ePin."'")->fetchField();
                        $ePin_data = $connection->query("SELECT * FROM {ump_epins} WHERE epin_no='".$ePin."'")->fetchAll();                        
                    }
                    
                    if (!empty($ePin_a)) {
                        if ($ePin_data->status == 0) {                             
                            $current_date= date('Y-m-d h:i:s', \Drupal::time()->getCurrentTime());
                            $connection->update('ump_epins')->fields([
                                'user_id'=>"$user_id",
                                'date_used'=>"$current_date",
                                'status'=>'1'
                            ])->condition('epin_no',$ePin,'=')
                            ->execute();

                        } else {                             
                            $connection->update("ump_epins")->fields([
                                'user_key'=>"$user_id",
                              'date_used'=>"$current_date"  
                            ])->condition('epin_no',$ePin,'=')
                            ->execute();  
                        }                       
                        
                        $connection->update("ump_user")->fields([
                            'payment_status'=>"0"                               
                            ])->condition('user_id',$user_id,'=')
                            ->execute();
                            
                            $ePin_data = $connection->query("SELECT * FROM {ump_epins} WHERE epin_no='".$ePin."'")->fetchObject();
                             
                            if ($ePin_data->type == 'paid') {
                                // if (isset($ump_general_settings['activate_repurchse']) && $ump_general_settings['activate_repurchse'] == 'on') {
                                    //     $validity = $ePin_data->epin_validity;
                                    //     $date = strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . "+" . $validity . " months");
                                    //     $date = date("Y-m-d", $date);
                                    // } else {
                                        //     $date = '';
                                        // }                                                          
                                $update_current_date= date('Y-m-d h:i:s', \Drupal::time()->getCurrentTime());                                
                                $query_data=$connection->update("ump_user")->fields([
                                'payment_status'=>"1",
                                'payment_date'=>"$update_current_date",                                                                                          
                                ])->condition('user_id',$user_id,'=')
                                ->execute();                                
                                
                                if ($query_data) {                                 
                                 ump_distibute_epin_commission($ePin_data, $user_id);
                            }
                        }
                    }
            }
            
            ump_join_mail_function($user_id);

        return $this->messenger()->addStatus($this->t('User has been Join Successfully.'));
    }
}