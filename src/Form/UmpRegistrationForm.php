<?php
namespace Drupal\unilevelmlm\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Url;
use Drupal\Core\Routing;
use Drupal\user\RoleInterface;
use Drupal\user\RoleStorageInterface;
use Drupal\user\Entity\User;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InsertCommand;
use Drupal\Core\Ajax\CssCommand;
 

class UmpRegistrationForm extends ConfigFormBase {

    public function getFormId()
    {
        return 'unilevelmlm_ump_registration_config_form';
    }

    protected function getEditableConfigNames()
    {
          
        return [
            'unilevelmlm.ump_registration'
        ];
    }
        

    public function buildForm(array $form, FormStateInterface $form_state, $referral_id=NULL)
    {
       
        $config = $this->config('unilevelmlm.ump_registration');   
        $connection=\Drupal::service('database');        
        
        $form['ump_registration'] = array(
            '#type' => 'details',
            '#title' => t('Registration Form'),
            '#open' => TRUE,               
          );

        $form['ump_registration']['ump_user_name'] = [
            '#type' => 'textfield',
            '#title' => t('User Name'),            
            '#required' => TRUE,             
            '#default_value' => $config->get('ump_user_name'),
            '#placeholder'=>t('User name'),                           
            '#attributes'	=> array('class' => array('let-form-control w-95')),
            '#wrapper_attributes' => ['class' => ['let-col-md-6 let-col-sm-6']],
            '#prefix'=>'<div class="let-row let-mb-n3">',
            '#field_suffix'=>'<span id="edit-ump-user-name-result"></span>',
            '#ajax'=>[
            'callback'=>[$this, 'user_name_exist'],                                
            'disable-refocus' => TRUE,
            'event' => 'blur',             
            'progress' => [
                'type' => 'throbber',
                'message' => $this->t('Verifying User Name'),
              ],
            ],
                         
                            
        ];          
        

        $form['ump_registration']['ump_user_email'] = [
              '#type' => 'email',
              '#title' => t('User Email'),   
              '#required' => TRUE,            
              '#default_value' => $config->get('ump_user_email'),
              '#placeholder'=>t('example@test.com'), 
              '#field_suffix'=>'<span id="edit-ump-user-email-result"></span>'              ,
              '#ajax'=>[
                'callback'=>[$this, 'user_email_exist'],                                
                'disable-refocus' => TRUE,
                'event' => 'blur',             
                'progress' => [
                    'type' => 'throbber',
                    'message' => $this->t('Verifying User Email'),
                  ],
                ],               
                '#wrapper_attributes' => ['class' => ['let-col-md-6 let-col-sm-6']],				 
				'#attributes'	=> array('class' => array('let-form-control w-95')),
				'#suffix'=>'</div>'                
        ];

        $form['ump_registration']['ump_user_password'] = [
            '#type' => 'password',
            '#title' => t('User Password'),
            '#required' => TRUE,
            '#default_value' => $config->get('ump_user_password'),
            '#placeholder'=>t('User password') ,             
            '#attributes'	=> array('class' => array('let-form-control w-95')),
            '#wrapper_attributes' => ['class' => ['let-col-md-6 let-col-sm-6']],
            '#prefix'=>'<div class="let-row let-mb-n3">',                        
        ];

        $form['ump_registration']['ump_user_confirm_password'] = [
            '#type' => 'password',
            '#title' => t('User confirm password'),
            '#required' => TRUE,
            '#default_value' => $config->get('ump_user_confirm_password'),
            '#placeholder'=>t('User confirm password'),             
            '#ajax'=>[
                'callback'=>[$this, 'user_confirm_password_valid'],                                
                'disable-refocus' => TRUE,
                'event' => 'blur',             
                'progress' => [
                    'type' => 'throbber',
                    'message' => $this->t('Verifying User Confirm Password'),
                  ],
                ],
                '#wrapper_attributes' => ['class' => ['let-col-md-6 let-col-sm-6']],				 
                '#field_suffix'=>'<span id="edit-ump-user-confirm_password-result"></span>',
				'#attributes'	=> array('class' => array('let-form-control w-95')),
				'#suffix'=>'</div>'
            
        ];

        $form['ump_registration']['ump_sponsor'] = [
            '#type' => 'textfield',
            '#title' => t('Sponsor'),
            '#required' => TRUE,
            '#default_value' => (!empty($referral_id))?ump_get_child_user_name_by_id($referral_id):$config->get('ump_sponsor'),
            '#placeholder'=>t('Fill Sponsor'),             
            '#ajax'=>[
                'callback'=>[$this, 'check_sponsor_valid'],                                
                'disable-refocus' => TRUE,
                'event' => 'blur',             
                'progress' => [
                    'type' => 'throbber',
                    'message' => $this->t('Verifying Sponsor'),
                  ],
                ],
            '#attributes'	=> array('class' => array('let-form-control w-95')),
            '#wrapper_attributes' => ['class' => ['let-col-md-6 let-col-sm-6']],
            '#prefix'=>'<div class="let-row let-mb-n3">', 
            '#field_suffix'=>'<span id="edit-ump-sponsor-result"></span>'
            
        ];
        
        $form['ump_registration']['ump_ePin'] = [
            '#type' => 'textfield',
            '#title' => t('ePin'),             
            '#default_value' => $config->get('ump_ePin'),
            '#placeholder'=>t('Fill ePins'),
            '#attributes'	=> array('class' => array('form-control w-25')),
            '#ajax'=>[
                'callback'=>[$this, 'check_ePin_valid'],                                
                'disable-refocus' => TRUE,
                'event' => 'blur',             
                'progress' => [
                    'type' => 'throbber',
                    'message' => $this->t('Verifying ePins'),
                  ],
                ],
                '#wrapper_attributes' => ['class' => ['let-col-md-6 let-col-sm-6']],				 
                '#field_suffix'=>'<span id="edit-ump-ePin-result"></span><br>',                 
				'#attributes'	=> array('class' => array('let-form-control w-95')),
				'#suffix'=>'</div>'
        ];

        $form['ump_registration']['actions']['#type'] = 'actions';
        $form['ump_registration']['submit'] = [
            '#type' => 'submit',
            '#value' => t('Register User'),
            '#attributes'	=> array('class' => array('m-auto d-flex')),
        ];
        $form['ump_registration']['submit']['#attributes']['style'][]='background:#6262ce; color:white; height:35px; border:#6262ce;';

        return $form;
    }
    
    function user_name_exist(array &$form, FormStateInterface $form_state){
        $ajax_response = new AjaxResponse();
        $user_name=$form_state->getValue('ump_user_name');
        $result=check_user_name_esixt($user_name);         
        if($result>0)
        {            
            $value='';
            $text=t('User Name Already Exist');      
            $css = ['color' => 'red'];
        }else{
            // $form['ump_registration']['ump_user_name']['#value']=$user_name;            
            $text=t('User Name Availble');      
            $value=$user_name;
            $css = ['color' => 'green'];
        }  
        $ajax_response->addCommand(new HtmlCommand('#edit-ump-user-name-result', $text));
        $ajax_response->addCommand(new CssCommand('#edit-ump-user-name-result', $css));        
        $ajax_response->addCommand(new HtmlCommand('#edit-ump-user-name', $value));
        return $ajax_response;      
         
    }
    function user_email_exist(array &$form, FormStateInterface $form_state){
        $ajax_response = new AjaxResponse();
        $user_name=$form_state->getValue('ump_user_email');
        $result=check_email($user_name);         
        if(!empty($result))
        {            
            $value='';
            $text=t('User Email Already Exist');      
            $css = ['color' => 'red'];
        }else{
            // $form['ump_registration']['ump_user_name']['#value']=$user_name;            
            $text=t('User Email Availble');      
            $value=$user_name;
            $css = ['color' => 'green'];
        }  
        $ajax_response->addCommand(new HtmlCommand('#edit-ump-user-email-result', $text));
        $ajax_response->addCommand(new CssCommand('#edit-ump-user-email-result', $css));        
        // $ajax_response->addCommand(new HtmlCommand('#edit-ump-user-name', $value));
        return $ajax_response;           
    }
    function user_confirm_password_valid(array &$form, FormStateInterface $form_state){
        $ajax_response = new AjaxResponse();
        $ump_user_password=$form_state->getValue('ump_user_password');
        $ump_user_confirm_password=$form_state->getValue('ump_user_confirm_password');               
        if($ump_user_password!==$ump_user_confirm_password)
        {            
            $value='';
            $text=t('User Password is not Mached');      
            $css = ['color' => 'red'];
        }else{
            // $form['ump_registration']['ump_user_name']['#value']=$user_name;            
            $text=t('User Password is Mached');      
            $value=$user_name;
            $css = ['color' => 'green'];
        }  
        $ajax_response->addCommand(new HtmlCommand('#edit-ump-user-confirm_password-result', $text));
        $ajax_response->addCommand(new CssCommand('#edit-ump-user-confirm_password-result', $css));        
        // $ajax_response->addCommand(new HtmlCommand('#edit-ump-user-name', $value));
        return $ajax_response;         
    }

    function check_sponsor_valid(array &$form, FormStateInterface $form_state){
        $ajax_response = new AjaxResponse();
        $ump_sponsor=$form_state->getValue('ump_sponsor');
        $sponsor_valid=ump_sponsor_exist_function($ump_sponsor);         
        if($sponsor_valid==0)
        {            
            $value='';
            $text=t('Sponsor is not Availble');      
            $css = ['color' => 'red'];
        }else{
            // $form['ump_registration']['ump_user_name']['#value']=$user_name;            
            $text=t('Sponsor is Availble');      
            $value=$user_name;
            $css = ['color' => 'green'];
        }  
        $ajax_response->addCommand(new HtmlCommand('#edit-ump-sponsor-result', $text));
        $ajax_response->addCommand(new CssCommand('#edit-ump-sponsor-result', $css));        
        // $ajax_response->addCommand(new HtmlCommand('#edit-ump-user-name', $value));
        return $ajax_response;        
    }

    function check_ePin_valid(array &$form, FormStateInterface $form_state){
        $ajax_response = new AjaxResponse();
        $ump_ePin=$form_state->getValue('ump_ePin');
        $valid_epin=ump_check_epins($ump_ePin);        
        if($valid_epin['status']==false || $valid_epin['status']==0)
        {
            $text=$valid_epin['message'];
            $css = ['color' => 'red'];
        }else{            
            $text=$valid_epin['message'];
            $css = ['color' => 'green'];
        }         
        $ajax_response->addCommand(new HtmlCommand('#edit-ump-ePin-result', $text));
        $ajax_response->addCommand(new CssCommand('#edit-ump-ePin-result', $css));        
        // $ajax_response->addCommand(new HtmlCommand('#edit-ump-user-name', $value));
        return $ajax_response;        
    }  
    

    public function submitForm(array &$form, FormStateInterface $form_state)
    {        
        $user_name=$form_state->getValue('ump_user_name');         
        $ump_user_email=$form_state->getValue('ump_user_email');
        $ump_user_password=$form_state->getValue('ump_user_password');
        $confirm_password=$form_state->getValue('ump_user_confirm_password');
        $sponsor_name=$form_state->getValue('ump_sponsor');
        $ePin=$form_state->getValue('ump_ePin');
        $messenger = \Drupal::messenger();
        $connection=\Drupal::service('database');

        $user_exist=check_user_name_esixt($user_name);
        if(!empty($user_exist))
        {
            return t($messenger->addMessage('User has already Exist', $messenger::TYPE_WARNING));            
        }

        $emailExist = check_email($ump_user_email);
        if (!empty($emailExist)) {
            return t($messenger->addMessage('Email is Already exists ', $messenger::TYPE_WARNING));            
        }

        if($ump_user_password!=$confirm_password){
            return t($messenger->addMessage('Password is not matched !', $messenger::TYPE_WARNING));
             
        } 

        $sponsor=ump_sponsor_exist_function($sponsor_name);        
        if($sponsor==0)
        {
            return t($messenger->addMessage('Sponsor is not Availble !', $messenger::TYPE_WARNING));
             
        }

        $valid_epin=ump_check_epins($ePin);
        
        if($valid_epin['status']==false || $valid_epin['status']==0)
        {
            return $messenger->addMessage($valid_epin['message'], $messenger::TYPE_WARNING);             
        }
        $config_refferal = \Drupal::config('unilevelmlm.general')->get('ump_referrals');
        $sponsor_id = $connection->query("SELECT uid FROM {users_field_data} WHERE name=:sponsor_name",['sponsor_name'=>$sponsor_name])->fetchField();
        $referrals = $connection->query("SELECT COUNT(*) FROM {ump_user} WHERE sponsor_key=:sponsor_id",['sponsor_id'=>$sponsor_id])->fetchField();
       
        if (!empty($sponsor_id)) {
            $mlm_key = ump_generateKey();
                do {
                    $count_key = $connection->query("SELECT COUNT(*) FROM {ump_user} 
                                                    WHERE mlm_key = '".$mlm_key."'")->fetchField();
                    $flag = 1;
                    if ($count_key == 1) {
                        $mlm_key = ump_generateKey();
                        $flag = 0;
                    }
                } while ($flag == 0);
                if ($config_refferal == 0) {
                    $parent_key = $sponsor_id;
                } else if ($config_refferal > $referrals) {
                    $parent_key = $sponsor_id;
                } else {
                    $parent_key =  ump_get_parent_key($sponsor_id);
                }
                $language = \Drupal::languageManager()->getCurrentLanguage()->getId();                 
                $user=\Drupal\user\Entity\User::create();        
                $user->enforceIsNew();
                $user->setUsername($user_name);
                $user->setEmail($ump_user_email);
                $user->setPassword($ump_user_password);
                $user->set("init", $ump_user_email);
                $user->set("langcode", $language);
                $user->set("preferred_langcode", $language);
                $user->set("preferred_admin_langcode", $language);      
                $user->activate();        
                $user->addRole('ump_user');       
                $user->save();
                $user_id=$user->id();                
                $mlm_key = ump_generateKey();         
                
                $current_date= date('Y-m-d h:i:s', \Drupal::time()->getCurrentTime());                       
                $n_row = $connection->query("SELECT n_row FROM {ump_user} WHERE user_id='".$parent_key."'")->fetchField();
                $n_row = $n_row + 1;             
                $connection->insert('ump_user')->fields([                 
                    'user_id' => $user_id,
                    'mlm_key'=> $mlm_key,		 				     
                    'parent_key'=> $parent_key,	
                    'sponsor_key' =>$sponsor_id,
                    'n_row'=>$n_row,			         
                    'payment_status' => "2",
                    'creation_date'=>$current_date,
                    'payment_date'=>$current_date
                    ])->execute();                
                
                    ump_insert_hirerchyrecord($user_id, $n_row); 
                
                    if (!empty($ePin)) {                         
                        $ePin_a = $connection->query("SELECT COUNT(*) FROM {ump_epins} WHERE epin_no=:epin_no",['epin_no'=>$ePin])->fetchField();
                        $ePin_data = $connection->query("SELECT * FROM {ump_epins} WHERE epin_no=:epin_no",['epin_no'=>$ePin])->fetchObject();                        
                    }
                    
                    if (!empty($ePin_a)) {
                        if ($ePin_data->status == 0) {                             
                            $current_date= date('Y-m-d h:i:s', \Drupal::time()->getCurrentTime());
                            $connection->update('ump_epins')->fields([
                                'user_id'=>$user_id,
                                'date_used'=>$current_date,
                                'status'=>'1'
                            ])->condition('epin_no',$ePin,'=')
                            ->execute();

                        } else {                             
                            $connection->update("ump_epins")->fields([
                                'user_id'=>$user_id,
                              'date_used'=>$current_date  
                            ])->condition('epin_no',$ePin,'=')
                            ->execute();  
                        }                       
                        
                        $connection->update("ump_user")->fields([
                            'payment_status'=>"0"                               
                            ])->condition('user_id',$user_id,'=')
                            ->execute();
                            
                            $ePin_data = $connection->query("SELECT * FROM {ump_epins} WHERE epin_no='".$ePin."'")->fetchObject();
                             
                            if ($ePin_data->type == 'paid') {
                                // if (isset($ump_general_settings['activate_repurchse']) && $ump_general_settings['activate_repurchse'] == 'on') {
                                    //     $validity = $ePin_data->epin_validity;
                                    //     $date = strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . "+" . $validity . " months");
                                    //     $date = date("Y-m-d", $date);
                                    // } else {
                                        //     $date = '';
                                        // }                                                          
                                $update_current_date= date('Y-m-d h:i:s', \Drupal::time()->getCurrentTime());                                
                                $query_data=$connection->update("ump_user")->fields([
                                'payment_status'=>"1",
                                'payment_date'=>"$update_current_date",                                                                                          
                                ])->condition('user_id',$user_id,'=')
                                ->execute();                                
                                
                                if ($query_data) {                                 
                                 ump_distibute_epin_commission($ePin_data, $user_id);
                            }
                        }
                    }
                    // mail send code
                    ump_register_mail_function($user_id);
                    
                                         
            }

        return $this->messenger()->addStatus($this->t('User has been Registered.'));
    }
}