<?php

namespace Drupal\unilevelmlm\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class EligibilityForm extends ConfigFormBase {

    public function getFormId()
    {
        return 'unilevelmlm_eligibility_config_form';
    }

    protected function getEditableConfigNames()
    {
        return [
            'unilevelmlm.eligibility'
        ];
    }
        
   
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config('unilevelmlm.eligibility');

        $form['eligibility'] = array(
            '#type' => 'details',
            '#title' => t('Eligibility Settings'),
            '#open' => FALSE, 
          );

        $form['eligibility']['ump_no_of_personal_referrals'] = [
            '#type' => 'number',
            '#title' => t('No. of Personal Referrer(s)'),
            '#required' => TRUE,
            '#description'=>t('Enter the No. of referrals'),
            '#default_value' => $config->get('ump_no_of_personal_referrals'),
            '#attributes'	=> array('class' => array('form-control w-25')),
        ];   
         
        $form['eligibility']['submit'] = [
            '#type' => 'submit',
            '#value' => t('Save Eligibility Settings'),
        ];

        $form['eligibility']['submit']['#attributes']['class'][]='button--primary';

        return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {

        $config = $this->config('unilevelmlm.eligibility');
        $config->set('ump_no_of_personal_referrals', $form_state->getValue('ump_no_of_personal_referrals'));         
        $config->save();
        return $this->messenger()->addStatus($this->t('Eligibility Setting has been Save.'));
    }
}