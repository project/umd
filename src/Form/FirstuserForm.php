<?php
namespace Drupal\unilevelmlm\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Url;
use Drupal\Core\Routing;
use Drupal\user\RoleInterface;
use Drupal\user\RoleStorageInterface;
use Drupal\user\Entity\User;

class FirstuserForm extends ConfigFormBase {

    public function getFormId()
    {
        return 'unilevelmlm_firstuser_config_form';
    }

    protected function getEditableConfigNames()
    {          
        return [
            'unilevelmlm.firstuser'
        ];
    }
        

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config('unilevelmlm.firstuser');

        $form['firstuser'] = array(
            '#type' => 'details',
            '#title' => t('Create First User'),
            '#open' => TRUE, 
          );

        $form['firstuser']['ump_user_name'] = [
            '#type' => 'textfield',
            '#title' => t('User Name'),
            '#required' => TRUE,
            '#default_value' => $config->get('ump_user_name'),
            '#placeholder'=>t('User name'),
            '#attributes'	=> array('class' => array('form-control w-25')),
        ];          
        

        $form['firstuser']['ump_user_email'] = [
              '#type' => 'email',
              '#title' => t('user Email'),   
              '#required' => TRUE,            
              '#default_value' => $config->get('ump_user_email'),
              '#placeholder'=>t('example@test.com'),
              '#attributes'	=> array('class' => array('form-control w-25')),
                
        ];

        $form['firstuser']['ump_user_password'] = [
            '#type' => 'password',
            '#title' => t('User Password'),
            '#required' => TRUE,
            '#default_value' => $config->get('ump_user_password'),
            '#placeholder'=>t('User password'),
            '#attributes'	=> array('class' => array('form-control w-25')),
            
        ];

        $form['firstuser']['ump_user_confirm_password'] = [
            '#type' => 'password',
            '#title' => t('User confirm password'),
            '#required' => TRUE,
            '#default_value' => $config->get('ump_user_confirm_password'),
            '#placeholder'=>t('User confirm password'),
            '#attributes'	=> array('class' => array('form-control w-25')),
            
        ];

        $form['firstuser']['actions']['#type'] = 'actions';
        $form['firstuser']['submit'] = [
            '#type' => 'submit',
            '#value' => t('Save'),
        ];

        $form['firstuser']['submit']['#attributes']['class'][]='button--primary';

        return $form;
    }
    
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        $password=$form_state->getValue('ump_user_password');
        $confirm_password=$form_state->getValue('ump_user_confirm_password');
        $email=$form_state->getValue('ump_user_email');
        if($password!=$confirm_password){
            $form_state->setErrorByName('ump_user_confirm_password', $this->t('Password is not matched !'));
        }
        $emailExist = \Drupal::entityQuery('user')
            ->condition('mail', $email)
            ->execute();

        if (!empty($emailExist)) {
            $form_state->setErrorByName('ump_user_email', $this->t('Email is Already exists !'));
        } 
    }
    

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        
        $connection = \Drupal::service('database');
        $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $user_name=$form_state->getValue('ump_user_name');         
        $ump_user_email=$form_state->getValue('ump_user_email');
        $ump_user_password=$form_state->getValue('ump_user_password');       
        $user=\Drupal\user\Entity\User::create();        
        $user->enforceIsNew();
        $user->setUsername($user_name);
        $user->setEmail($ump_user_email);
        $user->setPassword($ump_user_password);
        $user->set("init", $ump_user_email);
        $user->set("langcode", $language);
        $user->set("preferred_langcode", $language);
        $user->set("preferred_admin_langcode", $language);      
        $user->activate();        
        $user->addRole('ump_user');       
        $user->save();
        $user_id=$user->id();                
        $user_key = ump_generateKey();         
        $current_date= date('Y-m-d h:i:s', \Drupal::time()->getCurrentTime());      
        $connection->insert('ump_user')
		  			->fields([                             
		  					'user_id' => "$user_id",
                            'mlm_key'=> "$user_key",		 				     
                            'parent_key'=> "0",	
                            'sponsor_key' =>"0",			         
                            'payment_status' => "1",
                            'creation_date'=>"$current_date",
							  ])
		  			->execute();
        return $this->messenger()->addStatus($this->t('First user has been created.'));
    }
}