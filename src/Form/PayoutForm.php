<?php
namespace Drupal\unilevelmlm\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class PayoutForm extends ConfigFormBase {

 
	public function getFormId()
	{
		return 'unilevelmlm_payout_config_form';
	}

	protected function getEditableConfigNames(){	 

		return [
			 'unilevelmlm.payout'
		];
	}

	public function buildForm(array $form , FormStateInterface $form_state ){
		$no_of_level = \Drupal::config('unilevelmlm.general')->get('ump_no_of_levels');
		$config = $this->config('unilevelmlm.payout');

		$form['payout'] = [
            '#type' => 'details',
            '#title' => t('Payout Settings'),
            '#open' => FALSE, 
          ];
		
		 

		$form['payout']['ump_join_commission']=[
			'#type'=>'number',
			'#title'=>t('Join Commission'),
			'#required' => TRUE,
			'#description'=>t('Enter Join commission'),
			'#default_value'=>$config->get('ump_join_commission'),
			'#attributes'	=> array('class' => array('let-form-control w-95')),
			'#wrapper_attributes' => ['class' => ['let-col-6']],
			'#prefix'=>'<div class="let-row">'
			
		];
		$form['payout']['ump_join_commission_type']=[			 
	        '#title' => t('Join Commission Type'),
	        '#type' => 'select',
	        '#description' => 'Select Commission Type.',
	        '#options' => array(
				'fixed'=>t('fixed'),
				'percent'=>t('percent')
			),
			'#wrapper_attributes' => ['class' => ['let-col-4']],
	        '#default_value' => $config->get('ump_join_commission_type'),
			'#attributes'	=> array('class' => array('let-form-control w-50')),
			'#suffix'=>'</div>'
		];

		 
		$form['payout']['ump_referral_commission']=[
			'#type'=>'number',
			'#title'=>t('Referral Commission'),
			'#required'=>TRUE,
			'#description'=>t('Refferal Commission'),
			'#default_value'=>$config->get('ump_referral_commission'),			 
			'#wrapper_attributes' => ['class' => ['let-col-6']],
			'#attributes'	=> array('class' => array('let-form-control w-95')),
			'#prefix'=>'<div class="let-row">'

		];
		$form['payout']['ump_referral_commission_type']=[			 
	        '#title' => t('Referral Commission Type'),
	        '#type' => 'select',
	        '#description' => 'Select Commission Type.',
	        '#options' => array(
	        'fixed'=>t('fixed'),
	         'percent'=>t('percent')
	     	),
			 '#wrapper_attributes' => ['class' => ['let-col-4']],
	        '#default_value' => $config->get('ump_referral_commission_type'),
			'#attributes'	=> array('class' => array('let-form-control w-50')),
			'#suffix'=>'</div>'
            

		];	 
		 
		$form['payout']['submit'] = [
            '#type' => 'submit',
            '#value' => t('Save Payout Settings'),
        ];

        $form['payout']['submit']['#attributes']['class'][]='button--primary let-m-auto let-d-flex';

        return $form;
	}

	public function submitForm(array &$form, FormStateInterface $form_state){
		$no_of_level = \Drupal::config('unilevelmlm.general')->get('ump_no_of_levels');
        \Drupal::configFactory()->getEditable('unilevelmlm.payout')->delete();
		$config = $this->config('unilevelmlm.payout');
        $config->set('ump_join_commission', $form_state->getValue('ump_join_commission'));
        $config->set('ump_join_commission_type', $form_state->getValue('ump_join_commission_type'));
        $config->set('ump_referral_commission', $form_state->getValue('ump_referral_commission'));
        $config->set('ump_referral_commission_type', $form_state->getValue('ump_referral_commission_type'));       
        $config->save();
        return $this->messenger()->addStatus($this->t('Payout Setting has been Save.'));
	}
}
?>