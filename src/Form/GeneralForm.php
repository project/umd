<?php

namespace Drupal\unilevelmlm\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Url;
use Drupal\Core\Routing;

class GeneralForm extends ConfigFormBase {

    public function getFormId()
    {
        return 'unilevelmlm_config_form';
    }

    protected function getEditableConfigNames()
    {         
        return [
            'unilevelmlm.general'
        ];
    }
        

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config('unilevelmlm.general');         
        $form['general'] = array(
            '#type' => 'details',
            '#title' => t('General Settings'),
            '#open' => FALSE, 
          );        
        
        $form['general']['ump_no_of_levels'] = [
            '#type' => 'number',
            '#title' => t('No. of Levels'),
            '#required' => TRUE,
            '#default_value' => $config->get('ump_no_of_levels'),
            '#attributes'	=> array('class' => array('form-control w-25')),
        ];
        
        
        $form['general']['ump_referrals'] = [
            '#type' => 'number',
            '#title' => t('Width of Nework'),
            '#required' => TRUE,
            '#default_value' => $config->get('ump_referrals'),
            '#attributes'	=> array('class' => array('form-control w-25')),
        ];
         
        
        $form['general']['activate_epin'] = [
            '#type' => 'checkbox',
            '#title' => t('Activate ePin'),
            '#default_value' => $config->get('activate_epin'),
        ];         
        
        $form['general']['ump_epin_length'] = [
            '#title' => t('ePin Length'),
            '#type' => 'select',
            '#description' => 'Select Your ePin Length.',
            '#default_value' => $config->get('ump_epin_length'),
            '#options' => [
                '6' => $this->t('6'),
                '8' => $this->t('8'),
                '10' => $this->t('10'),
                '12' => $this->t('12'),
            ],
            '#attributes'	=> array('class' => array('form-control w-25')),
        ];   
        
        
        $form['general']['ump_withdrawal_min_limit'] = [
            '#type' => 'number',
            '#title' => t('Minimum Withdrawal Limit'),
            '#default_value' => $config->get('ump_withdrawal_min_limit'),
            '#required' => TRUE,
            '#description'=>t('This is Minimum Winthdrawal Limit for the customer'),
            '#attributes'	=> array('class' => array('form-control w-25')),
        ];
         
        $form['general']['ump_withdrawal_max_limit'] = [
            '#type' => 'number',
            '#title' => t('Maximum Withdrawal Limit'),
            '#default_value' => $config->get('ump_withdrawal_max_limit'),
            '#required' => TRUE,
            '#description'=>t('This is Maximum Winthdrawal Limit for the customer'),
            '#attributes'	=> array('class' => array('form-control w-25')),
        ];
         
        
        $form['general']['actions']['#type'] = 'actions';
        $form['general']['submit'] = [
            '#type' => 'submit',
            '#value' => t('Save General Settings'),
        ];

       
        $form['general']['submit']['#attributes']['class'][]='button--primary';

        return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {

        $config = $this->config('unilevelmlm.general');
        $config->set('ump_no_of_levels', $form_state->getValue('ump_no_of_levels'));
        $config->set('ump_referrals', $form_state->getValue('ump_referrals'));
        $config->set('activate_epin', $form_state->getValue('activate_epin'));
        $config->set('ump_withdrawal_min_limit', $form_state->getValue('ump_withdrawal_min_limit'));
        $config->set('ump_withdrawal_max_limit', $form_state->getValue('ump_withdrawal_max_limit'));
        $config->set('ump_epin_length', $form_state->getValue('ump_epin_length'));
        $config->set('ump_no_of_row_of_bonus', $form_state->getValue('ump_no_of_row_of_bonus'));
        $config->save();         
        return $this->messenger()->addStatus($this->t('General Setting has been Save.'));

    }
}