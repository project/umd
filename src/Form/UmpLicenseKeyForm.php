<?php
namespace Drupal\unilevelmlm\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Url;
use Drupal\Core\Routing;
use Drupal\user\RoleInterface;
use Drupal\user\RoleStorageInterface;
use Drupal\user\Entity\User;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InsertCommand;
use Drupal\Core\Ajax\CssCommand;
 

class UmpLicenseKeyForm extends ConfigFormBase {

    public function getFormId()
    {
        return 'unilevelmlm_ump_license_key_setting_config_form';
    }

    protected function getEditableConfigNames()
    {          
        return [
            'unilevelmlm.ump_license_key_setting'
        ];
    }
        

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config('unilevelmlm.ump_license_key_setting'); 
        $test_mode = $this->config('unilevelmlm.test_mode_setting')->get('ump_license_key_host'); 
        $expiry_date=get_licence_expiry();
        // print_r($config->get('ump_license_key')); die;
        $form['ump_license_key_setting'] = array(
            '#type' => 'details',
            '#title' => t('License Key'),
            '#open' => TRUE, 
          );
        
          $form['ump_license_key_setting']['ump_license_key_expiry_date'] = [
            '#type' => 'html_tag',             
            '#tag'=>'p',
            '#value'=>t('Your License expiry date is '.$expiry_date)            
        ];
 
        $form['ump_license_key_setting']['ump_license_key'] = [
            '#type' => 'textfield',
            '#title' => t('License Key'),
            '#required' => TRUE,
            '#default_value' => $config->get('ump_license_key'),
            '#placeholder'=>t('Fill License key'),
            '#attributes'	=> array('class' => array('form-control w-25')),             
            
        ];       
        $form['ump_license_key_setting']['actions']['#type'] = 'actions';
        $form['ump_license_key_setting']['submit'] = [
            '#type' => 'submit',
            '#value' => t('Save'),
        ];
        $form['ump_license_key_setting']['submit']['#attributes']['style'][]='background:#6262ce; color:white; height:35px; border:#6262ce;';
        
        $form['ump_license_key_setting']['ump_license_key_purchase'] = [
            '#type' => 'html_tag',             
            '#tag'=>'a',
            '#value'=>t('Purchase License'),
            '#attributes'=>array('href'=>'https://mlmtrees.com','target'=>'_blank', 'class'=> array('let-btn let-btn-primary let-rounded-pill let-btn-sm let-text-white br-0 let-pointer'))             
        ];  
        if(!empty($test_mode) && $test_mode=='localhost'){
            $form['ump_license_key_setting']['ump_test_moding'] = [
                '#type' => 'submit',
                '#value' => $this->t("Test Mode Off"),
                '#submit' => ['::removeCallback'], 
                '#attributes'=>array('class'=> array('let-btn-success let-rounded-pill let-btn-sm br-0 let-pointer') )         
                
            ]; 
        }
        return $form;
    }   

    public function removeCallback(array &$form, FormStateInterface $form_state)
    {
        $test_config =\Drupal::service('config.factory')->getEditable('unilevelmlm.test_mode_setting');     
      $test_config->set('ump_license_key_host', '');
      $test_config->save();
      return $this->messenger()->addStatus($this->t('Test Mode is Off Successfully'));
    }
    public function submitForm(array &$form, FormStateInterface $form_state)
    {  
        $license_key=$form_state->getValue('ump_license_key');      
        $messenger = \Drupal::messenger();
        $connection=\Drupal::service('database');        
        $data=check_for_update_ump_license_key_setting($license_key);
        $config = $this->config('unilevelmlm.ump_license_key_setting');         
        
        if (!empty($data) && $data->success == true) {  
            $config->set('ump_license_key', $license_key); 
            $config->save();     
             
            $message = t("Your Licence is valid, Now you can enjoy the our plugin.
            Your licence Expiry Date is . (%expiry-date) . ", ['%expiry-date' =>date('d-M-Y', strtotime($data->expiry))]);           
            return $this->messenger()->addStatus($this->t('License is Update Successfully.'.$message.''));
        } else {
    
            $error=t('Your Licence Key is invalid, Please Enter valid licence key');
            return $messenger->addMessage($error, $messenger::TYPE_ERROR);
            
        }
    }
}