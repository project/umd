<?php
namespace Drupal\unilevelmlm\Controller;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Routing;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides route responses for the Unilevelmlm module.
 */
class GenealogyController extends ControllerBase {

  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function genealogy_function() {
    $current_user=\Drupal::currentUser();
    $user_id=$current_user->id();             
    $build['#theme'] = 'unilevelmlm_genealogy_template';     
    return $build;       
    
  }
}