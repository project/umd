<?php
namespace Drupal\unilevelmlm\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\user\Entity\User;
use Drupal\Core\Url; 
use Drupal\Core\Mail\MailManagerInterface;
// require "vendor/autoload.php";
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Drupal\file\Entity\File;

/**
 * Provides route responses for the Unilevelmlm module.
 */
class FrontendController extends ControllerBase {
  
  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function ump_registration()
  {    
     
   
      $registration_form = \Drupal::formBuilder()->getForm('Drupal\unilevelmlm\Form\UmpRegistrationForm');        
        
        return [
          '#attached' => array(
            'library' => array(
                'unilevelmlm/unilevelmlm',
            ),
          ),
          $registration_form
      ];
    
          
  }
 
  public function ump_registration_by_referral($id)
  {
     
    
       
      $registration_form = \Drupal::formBuilder()->getForm('Drupal\unilevelmlm\Form\UmpRegistrationForm',$id);        
        return [
          '#attached' => array(
            'library' => array(
                'unilevelmlm/unilevelmlm',
            ),
          ),
          $registration_form
      ];
              
  }

  public function ump_dashboard()
  {
     
      $current_user = \Drupal::currentUser();   
      $user_id =$current_user->id();           
      $user_name=ump_get_child_user_name_by_id($user_id);
      $data_menu_array=get_menu_array();     
      $earning_week_percent = 0;
      $withdrawal_week_percent=0;
      $user_week_percent=0;
      $logout=$GLOBALS['base_url'].'/user/logout';
      $profile_image=ump_get_profile_picture($user_id);
      $total_earning = get_total_earning($user_id);
      $total_earning_week = get_total_earing_week($user_id);    
      if(!empty($total_earning_week) && !empty($total_earning)) {
        $earning_week_percent = ($total_earning_week / $total_earning) * 100;
      } 
      $total_earning_week=($total_earning_week>0)?$total_earning_week:'0'; 
      $total_withdrawal=get_withdrawa_amount_earning($user_id);   
      $total_withdrawal_week=get_withdrawa_week($user_id);
      if(!empty($total_withdrawal) && !empty($total_withdrawal_week)) {
        $withdrawal_week_percent = ($total_withdrawal_week / $total_withdrawal) * 100;
      } 
      $total_withdrawal=($total_withdrawal>0)?$total_withdrawal:'0';

      $total_downlines=get_total_downlines($user_id);   
      $total_downlines_week=get_total_downlines_week($user_id);
      if(!empty($total_downlines) && !empty($total_downlines_week)) {
        $user_week_percent = ($total_downlines_week / $total_downlines) * 100;
      } 
      $total_downlines=($total_downlines>0)?$total_downlines:'0';   
      
       
      
      return array(
        '#theme' => 'unilevelmlm_dashboard_template',       
        '#menu_array'=>$data_menu_array,
        '#current_user_name'=>$user_name,
        '#total_earning_week'=>$total_earning_week,
        '#earning_week_percent'=>number_format($earning_week_percent,0),
        '#total_withdrawal'=>$total_withdrawal,
        '#withdrawal_week_percent'=>$withdrawal_week_percent,
        '#total_downlines'=> $total_downlines,
        '#user_week_percent'=>$user_week_percent,        
        '#logout'=>$logout,
        '#profile_image'=>$profile_image        
          );  
          
  }

    public function payout_detail(){       
    $current_user = \Drupal::currentUser();
    $user_id =$current_user->id(); 
    
      $user_name=ump_get_child_user_name_by_id($user_id);
      $logout=$GLOBALS['base_url'].'/user/logout';
      $profile_image=ump_get_profile_picture($user_id);
      $data_menu_array=get_menu_array(); 
      $payout_detail=array()     ;
      $payout_detail=ump_get_payout_detail_list($user_id);     
      return array(
        '#theme' => 'unilevelmlm_frontpayout_template',
        '#menu_array'=>$data_menu_array,
        '#current_user_name'=>$user_name,
        '#payout_detail'=>$payout_detail,
        '#logout'=>$logout,
        '#profile_image'=>$profile_image 
      );
    
  }
    public function join_commission(){
                
        $current_user = \Drupal::currentUser();
        $user_id =$current_user->id();
        $logout=$GLOBALS['base_url'].'/user/logout';
        $profile_image=ump_get_profile_picture($user_id);
        $user_name=ump_get_child_user_name_by_id($user_id);
        $data_menu_array=get_menu_array(); 
        $join_comm=array();
        $join_commissions=ump_join_commission_by_userId($user_id);
        $i=0;
        if(!empty($join_commissions))
        {
        
          foreach($join_commissions as $value) 
          {
          $join_comm[$i]->id=$value->id;       
          $join_comm[$i]->user_name=ump_get_child_user_name_by_id($value->parent_id);
          $join_comm[$i]->child_name=ump_get_child_user_name_by_id($value->child_id);
          $join_comm[$i]->payout_id=$value->payout_id;
          $join_comm[$i]->date_notified=date_format(date_create($value->date_notified), ' jS M Y'); 
          $join_comm[$i]->comm_type=$value->comm_type;
          $join_comm[$i]->amount=$value->amount;
          $i++;
        }    
      } 
        
      return array(
        '#theme' => 'unilevelmlm_joincomm_template',
        '#menu_array'=>$data_menu_array,
        '#current_user_name'=>$user_name,
        '#join_commission'=>$join_comm,
        '#logout'=>$logout,
        '#profile_image'=>$profile_image 
      );
    
  }
  function ref_commission(){
    $current_user = \Drupal::currentUser();
    $user_id =$current_user->id();
    
      $user_name=ump_get_child_user_name_by_id($user_id);
      $data_menu_array=get_menu_array();  
      $ref_comm=array();
      $logout=$GLOBALS['base_url'].'/user/logout';
      $profile_image=ump_get_profile_picture($user_id);
      $ref_commission=ump_ref_commission_by_userId($user_id); 
      $i=0;
      if(!empty($ref_commission))
      {
        foreach($ref_commission as $value) 
        {
          $ref_comm[$i]->id=$value->id;
          $ref_comm[$i]->parent_id=$value->parent_id;
          $ref_comm[$i]->child_id=$value->child_id;
          $ref_comm[$i]->user_name=ump_get_child_user_name_by_id($value->parent_id);
          $ref_comm[$i]->child_name=ump_get_child_user_name_by_id($value->child_id);
          $ref_comm[$i]->payout_id=$value->payout_id;
          $ref_comm[$i]->date_notified=date_format(date_create($value->date_notified), ' jS M Y'); 
          $ref_comm[$i]->comm_type=$value->comm_type;
          $ref_comm[$i]->amount=$value->amount;
          $i++;
        }
      }
      return array(
        '#theme' => 'unilevelmlm_refcomm_template',
        '#menu_array'=>$data_menu_array,
        '#current_user_name'=>$user_name,
        '#ref_commission'=>$ref_comm,
        '#logout'=>$logout,
        '#profile_image'=>$profile_image 
      );
    
  }
  
  function account_detail(){    
    $current_user = \Drupal::currentUser();         
    $user_id =$current_user->id();
    
      $user_name=ump_get_child_user_name_by_id($user_id);
      $user_name=!empty($user_name)?$user_name:'';
      $data_menu_array=get_menu_array();  
      $user_info=array();
      $logout=$GLOBALS['base_url'].'/user/logout';
      $profile_image=ump_get_profile_picture($user_id);
      $user=ump_getUserInfoByuserid($user_id);
      $i=0;
      if(!empty($user)){
        foreach($user as $value) 
        {
          $user_info[$i]->user_id=$value->user_id;      
          $user_info[$i]->user_email=$current_user->getEmail();
          $user_info[$i]->join_date=$value->creation_date;
          $user_info[$i]->user_name=ump_get_child_user_name_by_id($value->user_id);
          $user_info[$i]->parent_key= $value->parent_key;
          $user_info[$i]->sponsor_key= $value->sponsor_key;
          $user_info[$i]->payment_date= date_format(date_create($value->payment_date), ' jS M Y'); 
          $i++;     
        }    
      }
        return array(
        '#theme' => 'unilevelmlm_personalinfo_template',
        '#menu_array'=>$data_menu_array,
        '#current_user_name'=>$user_name,
        '#user_info'=>$user_info,
        '#logout'=>$logout,
        '#profile_image'=>$profile_image 
      );
    
  }

  function bank_detail(){

     
    $current_user = \Drupal::currentUser();         
    $user_id =$current_user->id();
    
      $user_name=ump_get_child_user_name_by_id($user_id);
      $data_menu_array=get_menu_array(); 
      $user_bank_details=array()   ;
      $logout=$GLOBALS['base_url'].'/user/logout';
      $profile_image=ump_get_profile_picture($user_id);
      $user_bank_details=ump_get_user_bank_details($user_id); 
      return array(
        '#theme' => 'unilevelmlm_bank_template',
        '#menu_array'=>$data_menu_array,
        '#current_user_name'=>$user_name,
        '#user_bank_details'=>$user_bank_details,
        '#logout'=>$logout,
        '#profile_image'=>$profile_image 
      ); 
     
  }

  function add_bank_detail(){    
    $current_user = \Drupal::currentUser();    
    $account_holder_name = $_POST['ump-bank-account-holder'];
    $account_number = $_POST['ump-bank-account-number'];
    $bank_name = $_POST['ump-bank-bank-name'];
    $branch = $_POST['ump-bank-branch-name'];
    $ifsc_code = $_POST['ump-bank-ifsc-code'];
    $contact_no = $_POST['ump-contact-number'];
    insert_user_bank_details($account_holder_name,$account_number,$bank_name,$branch,$ifsc_code,$contact_no,$current_user->id());  
     

  }

  function ump_withdrawal_amount(){
    $current_user = \Drupal::currentUser();         
    $user_id =$current_user->id();
    
      $user_name=ump_get_child_user_name_by_id($user_id);
      $data_menu_array=get_menu_array(); 
      $logout=$GLOBALS['base_url'].'/user/logout';
      $profile_image=ump_get_profile_picture($user_id);
      $min_limit = \Drupal::config('unilevelmlm.general')->get('ump_withdrawal_min_limit');
      $max_limit = \Drupal::config('unilevelmlm.general')->get('ump_withdrawal_max_limit');
      
      $total_amount =get_user_total_amount($user_id);    
      $processed_amount =get_user_processed_amount($user_id);
      $pending_amount = get_user_pending_amount($user_id);
      
      $remaining_balance = $total_amount - $processed_amount; 
      return array(
        '#theme' => 'unilevelmlm_withdrawalamount_template',
        '#menu_array'=>$data_menu_array,
        '#current_user_name'=>$user_name,
        '#total_amount'=>$total_amount,
        '#processed_amount'=>$processed_amount>0?$processed_amount:'0.00',
        '#pending_amount'=>$pending_amount>0?$pending_amount:'0.00',
        '#remaining_balance'=>number_format($remaining_balance,2),
        '#min_limit'=>!empty($min_limit)?$min_limit:0,
        '#max_limit'=>!empty($max_limit)?$max_limit:0,
        '#logout'=>$logout,
        '#profile_image'=>$profile_image 
      );
    
  }

  function ump_withdrawal_amount_by_user(){
    $current_user = \Drupal::currentUser();         
    $user_id =$current_user->id();        
    ump_withdrwal_amount_request_function($_POST['amount'], $user_id);    
  }
 

  function ump_downlines(){
    $current_user = \Drupal::currentUser();         
    $user_id =$current_user->id();
    
      $user_name=ump_get_child_user_name_by_id($user_id);
      $data_menu_array=get_menu_array();
      $logout=$GLOBALS['base_url'].'/user/logout';
      $profile_image=ump_get_profile_picture($user_id);
      return [
         
          '#attached' => array(
            'library' => array(
                'unilevelmlm/unilevelmlm',
            ),
          ),         
     
        '#theme' => 'unilevelmlm_frontgenealogy_template',
        '#menu_array'=>$data_menu_array,
        '#current_user_name'=>$user_name,
        '#logout'=>$logout,
        '#profile_image'=>$profile_image 

      ];
    
  }

  function ump_join(){               
      $join_form = \Drupal::formBuilder()->getForm('Drupal\unilevelmlm\Form\UmpJoinForm');   
      return [$join_form];
    
  } 
   
}