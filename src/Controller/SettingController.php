<?php
namespace Drupal\unilevelmlm\Controller;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Routing;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides route responses for the Unilevelmlm module.
 */
class SettingController extends ControllerBase {

  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function general() {    
  
    
    $general_settings = \Drupal::formBuilder()->getForm('Drupal\unilevelmlm\Form\GeneralForm');
    $eligibility_settings = \Drupal::formBuilder()->getForm('Drupal\unilevelmlm\Form\EligibilityForm');
    $payout_settings = \Drupal::formBuilder()->getForm('Drupal\unilevelmlm\Form\PayoutForm');     
    $ePin_generate = \Drupal::formBuilder()->getForm('Drupal\unilevelmlm\Form\EpinGenerateForm'); 
    $first_user = \Drupal::formBuilder()->getForm('Drupal\unilevelmlm\Form\FirstuserForm'); 
    $first_user_exist=get_first_user();
    
    if($first_user_exist>0){
      return [
        '#attached' => array(
          'library' => array(
              'unilevelmlm/unilevelmlm',
          ),
      ),      
        $general_settings, $eligibility_settings, $payout_settings, $ePin_generate];      
    }else{      
      return [$first_user];
    }
  
  }  

  public function distribute() {    
  $all_ump_users=get_all_ump_users();    
  $join_commission_array=array();
  $referral_commision_array=array();   
  $join_total_amount=0;
  $ref_total_amount=0;  

  foreach($all_ump_users as $key=>$user){ 
    $eligibility=ump_eligibility_check_for_commission($user->user_id);
    if($eligibility==1)
    {
      $user_name_array[]=[
        'key'=>$key+1,
        'user_id'=>$user->user_id,
        'user_name'=>ump_get_child_user_name_by_id($user->user_id),
      ];
      $user_join_commision=ump_user_join_commission_for_commission($user->user_id);      
      $user_referral_commision=ump_user_referral_commission_for_commission($user->user_id); 
       

      if(!empty($user_join_commision)){
        foreach($user_join_commision as $value){
          $join_child_user_name=ump_get_child_user_name_by_id($value->child_id);
          $join_total_amount+=$value->amount;
          $join_commission_array[]=[
            'child_id'=>$value->child_id,
            'amount'=>number_format($value->amount,2), 
            'child_name'=>$join_child_user_name                        
          ];         
        }       
        $join_total_amount=number_format($join_total_amount,2);
      }
        if(!empty($user_referral_commision)){
        foreach($user_referral_commision as $value){
          $ref_child_user_name=ump_get_child_user_name_by_id($value->child_id);
          $ref_total_amount+=$value->amount;
          $referral_commision_array[]=[
            'child_id'=>$value->child_id,
            'amount'=>number_format($value->amount,2),
            'parent_id'=>$value->parent_id,
            'child_name'=>$ref_child_user_name            
          ];          
        }
        $ref_total_amount=number_format($ref_total_amount,2);
      }    
         
    }      
  }
   

    return array(
      '#theme' => 'unilevelmlm_template',
      '#ump_user_name'=>$user_name_array,        
      '#join_commission'=>$join_commission_array,
      '#referral_commision'=>$referral_commision_array,       
      '#join_total_amount'=>$join_total_amount,
      '#ref_total_amount'=>$ref_total_amount,
    );
      
  }

  public function run_payout() {   
      
    $user_dataArray=ump_run_payout_display_functions();  
    return array(
      '#theme' => 'unilevelmlm_payout_template',
      '#data_array'=>$user_dataArray,
    );
  } 
  public function user_report() {
    $header = [
      'user_id' => $this->t('User Id'),
      'user_name' => $this->t('User Name'),
      'join_commission' => $this->t('Join Commission'),
      'referral_commission'=> $this->t('Referral Commission'),       
      'total_amount'=>$this->t('Total Amount'),
      'action'=>$this->t('Action')

      // 'Marks'=> $this->t('Marks'),	  
      // 'opt' =>$this->t('Operations')
      ];       
      $form['table'] = [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => get_user_report(),
        '#empty' => $this->t('No Users Found'),
      ];       
      $form['pager'] = [
        '#type' => 'pager'
      ];
      return [
        '#attached' => array(
          'library' => array(
              'unilevelmlm/unilevelmlm',
          ),
      ),  
      $form
    ]; 
  } 
  public function pay_report() {     
    $header = [
      'payout_id' => $this->t('Payout Id'),
      'user_name' => $this->t('User Name'),
      'join_commission' => $this->t('Join Commission'),
      'referral_commission'=> $this->t('Referral Commission'),       
      'total_amount'=>$this->t('Total Amount'),
      'action'=>$this->t('Action')

      // 'Marks'=> $this->t('Marks'),	  
      // 'opt' =>$this->t('Operations')
      ];       
      $form['table'] = [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => get_payout_report(),
        '#empty' => $this->t('No users found'),
      ];       
      $form['pager'] = [
        '#type' => 'pager'
      ];
      return [
        '#attached' => array(
          'library' => array(
              'unilevelmlm/unilevelmlm',
          ),
      ), 
      $form,
    ];
  } 

  public function genealogy() {
    return [
      '#markup' => 'Genealogy',
    
    ];
  }
  public function display_epins(){     
        $header = [
        'id' => $this->t('Id'),
        'epin_name' => $this->t('Epin Name'),
        'epin_price' => $this->t('ePin Price'),
        'type'=> $this->t('ePin Type'),
        'sold'=>$this->t('Sold/Total'),
        'create_date'=>$this->t('Create Date'),
        'action'=>$this->t('Action')

        // 'Marks'=> $this->t('Marks'),	  
        // 'opt' =>$this->t('Operations')
        ];       
        $form['table'] = [
          '#type' => 'table',
          '#header' => $header,
          '#rows' => get_epins(),
          '#empty' => $this->t('No users found'),
        ];       
        $form['pager'] = [
          '#type' => 'pager'
        ];
        return [
          '#attached' => array(
            'library' => array(
                'unilevelmlm/unilevelmlm',
            ),
        ), 
        $form,
      ];    
  }
  
  public function viewallepins($id){
    $header = [      
      'epin_no' => $this->t('ePin No'),
      'type'=>$this->t('Type'),
      'price'=>$this->t('Price'),
      'date_generated'=>$this->t('Date Generated'),
      'user_id'=>$this->t('User Id'),
      'date_used'=>$this->t('Date Used'),
      'status'=>$this->t('Status')     
    ];
    if(!empty($id)){
      $form['table'] = [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => get_all_epins($id),
        '#empty' => $this->t('No users found'),
      ]; 
    }
    $form['pager'] = [
      '#type' => 'pager'
    ];
    return [
      '#attached' => array(
        'library' => array(
            'unilevelmlm/unilevelmlm',
        ),
    ), 
    $form,
  ];   

  } 

  public function distribute_commission_function(){     
       ump_distribute_commission_function();      
      // echo json_encode($json);
      //                die;
  }
  public function run_payout_function(){   
    $json=ump_run_payout_functions();   
    echo json_encode($json);
    die;
     
  }

  function withdrawal(){
    $header = [
      'id' => $this->t('Id'),
      'user_id' => $this->t('User Id'),
      'user_name' => $this->t('User Name'),
      'withdrawal_initiated_date' => $this->t('Withdrawal Initiated Date'),
      'payment_processed_date'=> $this->t('Payment Processed Date'),
      'amount'=>$this->t('Requested Amount'),       
      'action'=>$this->t('Action')     
      ]; 
          
      
      $form['table'] = [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => withdrawal_list(),
        '#empty' => $this->t('No users found'),
         
      ];       
      $form['pager'] = [
        '#type' => 'pager'
      ];
      return [
        '#attached' => array(
          'library' => array(
              'unilevelmlm/unilevelmlm',
          ),
      ), 
      $form,
    ];
  }
  function transfer_amount($user_id,$id){
    $results=get_withdrawal_detail_by_wid($user_id, $id);
    $user_name=ump_get_child_user_name_by_id($user_id);
    $user_bank_info=ump_user_bank_detail($user_id);
    $history_info=[];
    $withdrawal_history=ump_get_withdrawal_history($user_id);
    $i=0;
    foreach($withdrawal_history as $value){
      $history_info[$i]->amount=$value->amount;      
      $history_info[$i]->withdrawal_initiated_date=date_format(date_create($value->withdrawal_initiated_date), ' jS M Y');
      $history_info[$i]->payment_processed_date=(!empty($value->payment_processed_date))?$value->payment_processed_date:'0000-00-00';
      $history_info[$i]->payment_mode=ucwords(str_replace('_', ' ', $value->payment_mode));
      $history_info[$i]->transaction_id= $value->transaction_id;
      $history_info[$i]->user_bank_name= $value->user_bank_name;
      $history_info[$i]->user_bank_account_no= $value->user_bank_account_no; 
      $history_info[$i]->payment_processed= ($value->payment_processed=='0')?'Intiated':'Processed'; 
      $i++;
    }    
    return array(
      '#theme' => 'unilevelmlm_admin_transferamount_template',
      '#data'=>$results,
      '#user_name'=>$user_name,
      '#bank'=>$user_bank_info,
      '#withdrawal_history'=>$history_info,       
    );
  }

  function transfer_user_amount(){    
    $amount=$_POST['ump_withdrawal_pay_amount'];
    $user_id=$_POST['user_id'];
    $id=$_POST['id'];
    $ump_withdrawal_payment_mode=$_POST['ump_withdrawal_payment_mode'];
    $ump_withdrawal_transaction_id=$_POST['ump_withdrawal_transaction_id'];
    ump_withdrwal_request_approve_function($amount,$user_id,$id,$ump_withdrawal_payment_mode,$ump_withdrawal_transaction_id);
      
  }
  function user_report_data($user_id){
    $join_comm=[]; $ref_comm=[]; $level_comm=[]; $reg_bonus=[];
    $current_user=\Drupal::currentUser();
    $email=get_user_email_by_id($user_id);
    $phone=get_phone_number($user_id);
    $user_name=ump_get_child_user_name_by_id($user_id);
    $image=ump_get_profile_picture($user_id);
    $total_balance = get_user_total_amount($user_id);
    $withdrawal_amount = get_user_processed_amount($user_id);
    $downlines=get_user_downlines($user_id);
    $user_info=get_user_info($user_id);
    $current_balance = $total_balance - $withdrawal_amount;
    $profile_info['user_name']=$user_name;
    $profile_info['image']= $image;
    $profile_info['current_balance']= number_format($current_balance,2);    
    $profile_info['downlines']= $downlines;
    $profile_info['withdrawal_amount']= (!empty($withdrawal_amount))?number_format($withdrawal_amount):0;
    $join_commission=ump_join_commission_by_userId($user_id);
    $referral_commission=ump_ref_commission_by_userId($user_id);          
    $join_comm=get_usable_data($join_commission);
    $ref_comm=get_usable_data($referral_commission);     
     
    return [
      '#theme'=>'unilevelmlm_userreport_template',          
      '#profile_info'=>$profile_info,
      '#user_info'=>$user_info,
      '#email'=>$email,
      '#ump_phone'=>$phone,
      '#join_commission'=>$join_comm,
      '#referral_commission'=>$ref_comm,       
       
    ];
  }

  function pay_report_data($payout_id){
    $join_comm=[]; $ref_comm=[]; $level_comm=[]; $reg_bonus=[];        
    $join_commission=ump_join_commission_by_payoutId($payout_id);
    $referral_commission=ump_ref_commission_by_payoutId($payout_id);        
    $join_comm=get_usable_data($join_commission);
    $ref_comm=get_usable_data($referral_commission);     
    return [
      '#theme'=>'unilevelmlm_payoutreport_template',          
      '#join_commission'=>$join_comm,
      '#referral_commission'=>$ref_comm,       
    ];

  }    
}